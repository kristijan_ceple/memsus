import atexit
import os
import signal
import string
import sys
from collections import namedtuple
import subprocess
import json
import typing
from enum import Enum

########################################################################################################################
# CONSTANTS AND GLOBAL DEFINITIONS/OBJECTS  ############################################################################
########################################################################################################################
Resolution = namedtuple('Resolution', ['width', 'height'])

DEFAULT_WIDTH = 1920
DEFAULT_HEIGHT = 1080
DEFAULT_TARGET = "./cmake-build-debug/matrix_mult_n"
DEFAULT_D1_SIZE_BYTES = 32768; DEFAULT_D1_ASSOC = 8; DEFAULT_D1_LINE_SIZE_BYTES = 64
DEFAULT_I1_SIZE_BYTES = 65536; DEFAULT_I1_ASSOC = 4; DEFAULT_I1_LINE_SIZE_BYTES = 64
DEFAULT_LL_SIZE_BYTES = 4194304; DEFAULT_LL_ASSOC = 16; DEFAULT_LL_LINE_SIZE_BYTES = 64
MATRIX_N = 100

valgrind_cmd = "valgrind --tool=cachegrind --cachegrind-out-file={out_file}" \
               " --log-file={log_file}" \
               " {additional_args} {target} {width} {height} {width_other} {height_other}"
cg_annotate_cmd = "cg_annotate --auto=yes {filename}"

PROGRESS_TRACK_FILE_NAME = "progress_track.json"
GLOBAL_PROGRESS = None
PROGRESS_TRACK_FILE: typing.IO[str] = None


class CacheType(Enum):
    D1 = {
        "cache_name": "d1",
        "cache_size_bytes": DEFAULT_D1_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_D1_ASSOC,
        "cache_line_size_bytes": DEFAULT_D1_LINE_SIZE_BYTES
    }
    I1 = {
        "cache_name": "i1",
        "cache_size_bytes": DEFAULT_I1_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_I1_ASSOC,
        "cache_line_size_bytes": DEFAULT_I1_LINE_SIZE_BYTES
    }
    LL = {
        "cache_name": "ll",
        "cache_size_bytes": DEFAULT_LL_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_LL_ASSOC,
        "cache_line_size_bytes": DEFAULT_LL_LINE_SIZE_BYTES
    }


class Parameter(Enum):
    TOTAL_SIZE = {"desc": "Total Sizes", "id": "total_size"}
    ASSOC = {"desc": "Associativity", "id": "assoc"}
    LINE_SIZE = {"desc": "Line Sizes", "id": "line_size"}
    # RESOLUTION = {"desc": "Resolutions", "id": "res"}


image_resolutions = {
    "144p": {
        "width": 256,
        "height": 144
    },
    "240p": {
        "width": 426,
        "height": 240
    },
    "360p": {
        "width": 480,
        "height": 360
    },
    "480p": {
        "width": 640,
        "height": 480
    },
    "720p": {
        "width": 1280,
        "height": 720
    },
    "1080p": {
        "width": 1920,
        "height": 1080
    },
    "4K": {
        "width": 3840,
        "height": 2160
    },
    "8K": {
        "width": 7680,
        "height": 4320
    },
}
########################################################################################################################
# CONSTANTS AND GLOBAL DEFINITIONS/OBJECTS  ############################################################################
########################################################################################################################


########################################################################################################################
# DATA GENERATION METHODS  #############################################################################################
########################################################################################################################
def test_exec(cache: CacheType, parameter: Parameter, progress: dict, target_program_rel_path: string = DEFAULT_TARGET):
    cache_str, cache_default_total_size_bytes, cache_default_assoc, cache_default_line_size_bytes = cache.value.values()
    cache_str_upper = cache_str.upper()
    cache_default_lines_num = cache_default_total_size_bytes // cache_default_line_size_bytes

    par_desc, par_id = parameter.value.values()

    print(f"[INFO] Testing {cache_str_upper} Cache {par_desc}...")

    # Create output folder
    print("[INFO] Creating output folder...")
    output_folder = f"./valgrind_output/{cache_str}_{par_id}_test"
    os.makedirs(output_folder, exist_ok=True)

    target_program_name = target_program_rel_path.rsplit('/', 1).pop()

    upper_bound = None
    lower_bound = 1
    if parameter is Parameter.TOTAL_SIZE:
        if cache is CacheType.LL:
            upper_bound = 256 * 1024 * 1024  # 256 MB
        elif cache in (CacheType.I1, CacheType.D1):
            upper_bound = 128 * 1024  # 128 kB
        else:
            return None
    elif parameter is Parameter.ASSOC:
        upper_bound = cache_default_lines_num
    elif parameter is Parameter.LINE_SIZE:
        upper_bound = 256
    else:
        return None

    curr_id = -1
    for curr_par_val in range(lower_bound, upper_bound + 1):
        curr_id += 1

        """
        If varying assoc, need to check whether cache_lines_num is divisible by assoc
        If varying line size or total size, need to make sure everything is divisible and proper
        Also need to check whether set count is a power of 2
        """
        additional_args_str = None  # --LX=<size>,<assoc>,<line_size>
        cache_curr_total_size_bytes = cache_default_total_size_bytes
        cache_curr_line_size_bytes = cache_default_line_size_bytes
        cache_curr_assoc = cache_default_assoc
        if parameter is Parameter.ASSOC:
            # if cache_default_lines_num % curr_par_val != 0:
            #     continue
            cache_curr_assoc = curr_par_val
            additional_args_str = f"--{cache_str_upper}={cache_default_total_size_bytes},{curr_par_val}," \
                                  f"{cache_default_line_size_bytes}"
        elif parameter is Parameter.TOTAL_SIZE:
            # Check whether par is divisible by line_size and assoc
            # if curr_par_val % (cache_default_line_size_bytes * cache_default_assoc) != 0:
            #     continue
            cache_curr_total_size_bytes = curr_par_val
            additional_args_str = f"--{cache_str_upper}={curr_par_val},{cache_default_assoc}," \
                                  f"{cache_default_line_size_bytes}"
        elif parameter is Parameter.LINE_SIZE:
            # Check whether total_size is divisible by (par * assoc)
            # if cache_default_total_size_bytes % (cache_default_assoc * curr_par_val) != 0:
            #     continue
            cache_curr_line_size_bytes = curr_par_val
            additional_args_str = f"--{cache_str_upper}={cache_default_total_size_bytes},{cache_default_assoc}," \
                                  f"{curr_par_val}"
        else:
            return None

        # Check whether everything is okay right now
        if cache_curr_total_size_bytes % cache_curr_line_size_bytes != 0:
            continue
        cache_curr_lines_num = cache_curr_total_size_bytes // cache_curr_line_size_bytes
        if cache_curr_lines_num % cache_curr_assoc != 0:
            continue
        cache_curr_set_num = cache_curr_lines_num // cache_curr_assoc

        # Check if it is power of two
        if bin(cache_curr_set_num).count('1') != 1:
            print(f"[INFO] Not a power of 2, skipping current line size param({curr_par_val}). "
                  f"cache_curr_set_num = {cache_curr_set_num}")
            continue

        # All is fine. Run valgrind now if it hasnt been run already
        progress_id_str = f"{cache}-{parameter}-{curr_par_val}"
        if progress_id_str in progress and progress[progress_id_str] is True:
            print(f"[INFO] Skipping {progress_id_str} -> already done")
            continue

        cmd = valgrind_cmd.format(
            additional_args=additional_args_str,
            out_file=f"{output_folder}/{curr_id}_{par_id}={curr_par_val}_target={target_program_name}.out",
            log_file=f"{output_folder}/{curr_id}_{par_id}={curr_par_val}_target={target_program_name}.log",
            target=target_program_rel_path, width=MATRIX_N, height=MATRIX_N, width_other=MATRIX_N, height_other=MATRIX_N)
        print(f"[INFO] Running valgrind for parameter {par_desc}: {curr_par_val}, curr_id: {curr_id}...")
        print(f"[INFO] Running cmd: {cmd}")
        subprocess.run(cmd, shell=True, check=False, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL)
        progress[progress_id_str] = True
        PROGRESS_TRACK_FILE.truncate(0)
        PROGRESS_TRACK_FILE.seek(0)
        json.dump(progress, PROGRESS_TRACK_FILE)
        PROGRESS_TRACK_FILE.flush()

    print(f"[INFO] {par_desc} measurements done! Data saved!")


def test_resolutions(progress: dict, target_program_rel_path: string = DEFAULT_TARGET):
    print("[INFO] Testing various resolutions using default cache settings(CPUID)...")

    # Create output folder
    print("[INFO] Creating output folder...")
    output_folder = "./valgrind_output/resolution_test"
    os.makedirs(output_folder, exist_ok=True)

    target_program_name = target_program_rel_path.rsplit('/', 1).pop()

    for image_res, image_dims in image_resolutions.items():
        progress_id_str = f"Res.{image_res}"
        if progress_id_str in progress and progress[progress_id_str] is True:
            print(f"[INFO] Skipping {progress_id_str} -> already done")
            continue
        else:
            print(f"[INFO] Running valgrind for resolution: {image_res}({image_dims})...")

            cmd = valgrind_cmd.format(
                additional_args="",
                out_file=f"{output_folder}/{image_res}_target={target_program_name}.out",
                log_file=f"{output_folder}/{image_res}_target={target_program_name}.log",
                target=target_program_rel_path, width=image_dims["width"], height=image_dims["height"],
                width_other="", height_other=""
            )
            print(f"[INFO] Running cmd: {cmd}")
            subprocess.run(cmd, shell=True, check=True, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL)

            progress[progress_id_str] = True
            PROGRESS_TRACK_FILE.truncate(0)
            PROGRESS_TRACK_FILE.seek(0)
            json.dump(progress, PROGRESS_TRACK_FILE)
            PROGRESS_TRACK_FILE.flush()

    print(f"[INFO] Resolution measurements FULLY completed! Data saved!")


########################################################################################################################
# DATA GENERATION METHODS  #############################################################################################
########################################################################################################################


########################################################################################################################
# DRIVER METHODS  ######################################################################################################
########################################################################################################################
def run_cache_params_tests(progress: dict):
    """
    First vary L1 cache -> D1 and I1 params
    Then vary L3 cache -> LL params

    For each of those 3 cache levels vary size, line_size and associativity
    Calling function:
        def test_exec(cache: CacheType, parameter: Parameter, target_program_name: string = DEFAULT_TARGET):

    Need to take into account that the running machine can suspend or pause execution. For that purpose, we need to
    remember where we left off.

    :return: Nothing, data is written and logged into the valgrind_output folder
    """
    for curr_cache_type in CacheType:
        for curr_parameter in Parameter:
            progress_id_str = f"{curr_cache_type}-{curr_parameter}"

            if progress_id_str in progress and progress[progress_id_str] is True:
                print(f"[INFO] Skipping {progress_id_str} -> already done")
                continue
            else:
                print(f"[INFO] Currently testing: {progress_id_str}")
                test_exec(curr_cache_type, curr_parameter, progress)
                progress[progress_id_str] = True
                PROGRESS_TRACK_FILE.truncate(0)
                PROGRESS_TRACK_FILE.seek(0)
                json.dump(progress, PROGRESS_TRACK_FILE)
                PROGRESS_TRACK_FILE.flush()

    print("[INFO] Cache parameters tests FULLY completed!")
    print("[INFO] Generating graphs...")


########################################################################################################################
# DRIVER METHODS  ######################################################################################################
########################################################################################################################


########################################################################################################################
# MAIN  ################################################################################################################
########################################################################################################################
def main():
    print("[INFO] Checking for already processed files...")
    progress = None
    global PROGRESS_TRACK_FILE
    try:
        PROGRESS_TRACK_FILE = open(PROGRESS_TRACK_FILE_NAME, "r+")
        print("[INFO] Progress file found - updating progress...")
        progress = json.load(PROGRESS_TRACK_FILE)

        if progress is None or type(progress) != dict:
            print("[ERROR] Wrong type of data located in the progress file!?!", file=sys.stderr)
            raise RuntimeError("Wrong data in file!")
    except (FileNotFoundError, json.JSONDecodeError) as e:
        print("[INFO] No progress done yet, will create a new progress file!")
        PROGRESS_TRACK_FILE = open(PROGRESS_TRACK_FILE_NAME, "w")
        progress = {"all": False}
        PROGRESS_TRACK_FILE.truncate(0)
        PROGRESS_TRACK_FILE.seek(0)
        json.dump(progress, PROGRESS_TRACK_FILE)
        PROGRESS_TRACK_FILE.flush()

    global GLOBAL_PROGRESS
    GLOBAL_PROGRESS = progress
    if progress["all"] is False:
        print("[INFO] Collecting data for the graphs...")
        run_cache_params_tests(progress)
        # test_resolutions(progress)
        test_matrix_n(progress)
        # test_lines(progress)
        print("[INFO] Data collected for the graphs!")

        progress["all"] = True
        PROGRESS_TRACK_FILE.truncate(0)
        PROGRESS_TRACK_FILE.seek(0)
        json.dump(progress, PROGRESS_TRACK_FILE)
        PROGRESS_TRACK_FILE.flush()
    else:
        print("[INFO] Data for graphing has already been collected!")


def test_matrix_n(progress: dict, target_program_rel_path: string = DEFAULT_TARGET):
    print("[INFO] Testing various Matrix sizes using default cache settings(CPUID)...")

    # Create output folder
    print("[INFO] Creating output folder...")
    output_folder = "./valgrind_output/matrix_n_test"
    os.makedirs(output_folder, exist_ok=True)

    target_program_name = target_program_rel_path.rsplit('/', 1).pop()

    for curr_N in range(3, 1000):
        progress_id_str = f"N.{curr_N}"
        if progress_id_str in progress and progress[progress_id_str] is True:
            print(f"[INFO] Skipping {progress_id_str} -> already done")
            continue
        else:
            print(f"[INFO] Running valgrind for matrix size: {curr_N}...")

            cmd = valgrind_cmd.format(
                additional_args="",
                out_file=f"{output_folder}/{curr_N}_target={target_program_name}.out",
                log_file=f"{output_folder}/{curr_N}_target={target_program_name}.log",
                target=target_program_rel_path, width=curr_N, height=curr_N,
                width_other=curr_N, height_other=curr_N
            )
            print(f"[INFO] Running cmd: {cmd}")
            subprocess.run(cmd, shell=True, check=True, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL)

            progress[progress_id_str] = True
            PROGRESS_TRACK_FILE.truncate(0)
            PROGRESS_TRACK_FILE.seek(0)
            json.dump(progress, PROGRESS_TRACK_FILE)
            PROGRESS_TRACK_FILE.flush()

    print(f"[INFO] Resolution measurements FULLY completed! Data saved!")


def test_lines(progress: dict):
    for curr_cache_type in CacheType:
        curr_parameter = Parameter.LINE_SIZE
        progress_id_str = f"{curr_cache_type}-{curr_parameter}"
        test_exec(curr_cache_type, curr_parameter, progress)


def release():
    # PROGRESS_TRACK_FILE.close()
    print("[EXIT] Releasing resources...")
    global PROGRESS_TRACK_FILE
    PROGRESS_TRACK_FILE.truncate(0)
    PROGRESS_TRACK_FILE.seek(0)
    json.dump(GLOBAL_PROGRESS, PROGRESS_TRACK_FILE)
    PROGRESS_TRACK_FILE.close()             # Automatically flushes


def signal_handler(signo, frame):
    release()


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGHUP, signal_handler)
    atexit.register(release)
    main()
