#include <iostream>
#include <random>
#include <functional>
#include <chrono>
using namespace std;

/**
 * Koristi se za indeksiranje elemenata u pixelima
 */
enum class RGB: uint_fast8_t {R = 0, G = 1, B = 2};
enum class YCbCr: uint_fast8_t {Y = 0, Cb = 1, Cr = 2};

struct Pixel {
    double elements[3];
    double& operator[](int index) {
        return elements[index];
    }
};

//constexpr int WIDTH = 1920;
//constexpr int HEIGHT = 1080;
int HEIGHT, WIDTH;
Pixel* image;

/**
 * Random generator, koristi se:
 *      double broj = genRand();
 */
unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
default_random_engine generator(seed);
uniform_real_distribution<double> distribution(0,255);
auto genRand = bind(distribution, generator);

void generateImage() {
    // Prvo treba generirati sliku s random elementima
    for(int i = 0; i < HEIGHT; i++) {
        for(int j = 0; j < WIDTH; j++) {
            auto& pixel = image[i*WIDTH + j];
            pixel[(uint_fast8_t)RGB::R] = genRand();
            pixel[(uint_fast8_t)RGB::G] = genRand();
            pixel[(uint_fast8_t)RGB::B] = genRand();
        }
    }
}

vector<array<int, 2>> generateConversionIndices() {
    vector<array<int, 2>> indices;

    for(int i = 0; i < HEIGHT; i++) {
        for(int j = 0; j < WIDTH; j++) {
            indices.push_back({i, j});
        }
    }

    return move(indices);
}

vector<array<int, 3>> generateSummingIndices() {
    vector<array<int, 3>> indices;

    for(int i = 0; i < HEIGHT; i++) {
        for(int j = 0; j < WIDTH; j++) {
            indices.push_back({i, j, 0});
            indices.push_back({i, j, 1});
            indices.push_back({i, j, 2});
        }
    }

    return move(indices);
}

void conversionAndSum(vector<array<int, 2>>& convertIndices, vector<array<int, 3>>& sumIndices) {
      // Convert
    for(const auto& index : convertIndices) {
        const auto i = index[0];
        const auto j = index[1];

        double R = image[i*WIDTH + j][(uint_fast8_t)RGB::R];
        double G = image[i*WIDTH + j][(uint_fast8_t)RGB::G];
        double B = image[i*WIDTH + j][(uint_fast8_t)RGB::B];

        image[i*WIDTH + j][(uint_fast8_t)YCbCr::Y] = 0.299*R + 0.587*G + 0.114*B;
        image[i*WIDTH + j][(uint_fast8_t)YCbCr::Cb] = -0.1687*R - 0.3313*G + 0.5*B + 128;
        image[i*WIDTH + j][(uint_fast8_t)YCbCr::Cr] = 0.5*R - 0.4187*G - 0.0813*B + 128;
    }

    // Sumiranje
    double sum = 0;
    for(const auto& index : sumIndices) {
        const auto i = index[0];
        const auto j = index[1];
        const auto k = index[2];

        sum += image[i*WIDTH + j][k];
    }
    cout << "Suma = " << sum << endl;
}


int main(int argc, char *argv[]) {
//    if(argc != 3) {
//        cerr << "Exactly 2 arguments have to be passed to the program -> Height, Width";
//        return -1;
//    }
//
//    HEIGHT = strtol(argv[1], nullptr, 10);
//    WIDTH = strtol(argv[2], nullptr, 10);

    WIDTH = 3840;
    HEIGHT = 2160;
    image = new Pixel[HEIGHT * WIDTH];

    cout << "Zadatak A" << endl;
    generateImage();

    // 1. Poredani indeksi za sekvencijalno citanje i pisanje
    vector<array<int, 2>> orderedConvertIndices = generateConversionIndices();
    vector<array<int, 3>> orderedSummingIndices = generateSummingIndices();

    // 2. Razbacani indeksi za nasumicno citanje i pisanje
    vector<array<int, 2>> randomConvertIndices = orderedConvertIndices;
    vector<array<int, 3>> randomSummingIndices = orderedSummingIndices;
    shuffle(randomConvertIndices.begin(), randomConvertIndices.end(), generator);
    shuffle(randomSummingIndices.begin(), randomSummingIndices.end(), generator);

    cout << "Sekvencijalno citanje i pisanje: " << endl;
    auto start = chrono::high_resolution_clock::now();
    conversionAndSum(orderedConvertIndices, orderedSummingIndices);
    auto stop = chrono::high_resolution_clock::now();
    cout << "Trajanje: " << chrono::duration_cast<chrono::microseconds>(stop - start).count() << " ms" << endl << endl;

//    cout << "Nasumicno citanje i pisanje: " << endl;
//    auto start = chrono::high_resolution_clock::now();
//    conversionAndSum(randomConvertIndices, randomSummingIndices);
//    auto stop = chrono::high_resolution_clock::now();
//    cout << "Trajanje: " << chrono::duration_cast<chrono::microseconds>(stop - start).count() << " ms" << endl << endl;

    return 0;
}
