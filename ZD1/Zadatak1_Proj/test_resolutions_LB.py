def test_resolutions(progress: dict, target_program_rel_path: string = DEFAULT_TARGET):
    print("[INFO] Testing various resolutions using default cache settings(CPUID)...")

    # Create output folder
    print("[INFO] Creating output folder...")
    output_folder = "./valgrind_output/resolution_test"
    os.makedirs(output_folder, exist_ok=True)

    target_program_name = target_program_rel_path.rsplit('/', 1).pop()

    for image_res, image_dims in image_resolutions.items():
        progress_id_str = f"Res.{image_res}"
        if progress_id_str in progress and progress[progress_id_str] is True:
            print(f"[INFO] Skipping {progress_id_str} -> already done")
            continue
        else:
            print(f"[INFO] Running valgrind for resolution: {image_res}({image_dims})...")

            cmd = valgrind_cmd.format(
                additional_args="",
                out_file=f"{output_folder}/{image_res}_target={target_program_name}.out",
                log_file=f"{output_folder}/{image_res}_target={target_program_name}.log",
                target=target_program_rel_path, width=image_dims["width"], height=image_dims["height"]
            )
            print(f"[INFO] Running cmd: {cmd}")
            subprocess.run(cmd, shell=True, check=True, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL)

            progress[progress_id_str] = True
            PROGRESS_TRACK_FILE.truncate(0)
            PROGRESS_TRACK_FILE.seek(0)
            json.dump(progress, PROGRESS_TRACK_FILE)
            PROGRESS_TRACK_FILE.flush()

    print(f"[INFO] Resolution measurements FULLY completed! Data saved!")
