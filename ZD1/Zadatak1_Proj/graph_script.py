import matplotlib.pyplot as plt
import re
import os
from collections import namedtuple, defaultdict
from enum import Enum

########################################################################################################################
# CONSTANTS AND GLOBAL DEFINITIONS/OBJECTS  ############################################################################
########################################################################################################################
Resolution = namedtuple('Resolution', ['width', 'height'])
GraphPair = namedtuple('GraphPair', ['x', 'y'])

DEFAULT_WIDTH = 1920
DEFAULT_HEIGHT = 1080
DEFAULT_TARGET = "./cmake-build-debug/Zadatak1_Proj"
DEFAULT_D1_SIZE_BYTES = 32768; DEFAULT_D1_ASSOC = 8; DEFAULT_D1_LINE_SIZE_BYTES = 64
DEFAULT_I1_SIZE_BYTES = 65536; DEFAULT_I1_ASSOC = 4; DEFAULT_I1_LINE_SIZE_BYTES = 64
DEFAULT_LL_SIZE_BYTES = 4194304; DEFAULT_LL_ASSOC = 16; DEFAULT_LL_LINE_SIZE_BYTES = 64

valgrind_cmd = "valgrind --tool=cachegrind --cachegrind-out-file={out_file}" \
               " --log-file={log_file}" \
               " {additional_args} {target} {width} {height}"
cg_annotate_cmd = "cg_annotate --auto=yes {filename}"

HIT_PERCENTAGE_RE_PATTERN = re.compile(r"\d+\.\d+%")
GRAPHS_SAVE_FOLDER = "./valgrind_output_graphs"


class CacheType(Enum):
    D1 = {
        "cache_name": "d1",
        "cache_size_bytes": DEFAULT_D1_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_D1_ASSOC,
        "cache_line_size_bytes": DEFAULT_D1_LINE_SIZE_BYTES
    }
    I1 = {
        "cache_name": "i1",
        "cache_size_bytes": DEFAULT_I1_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_I1_ASSOC,
        "cache_line_size_bytes": DEFAULT_I1_LINE_SIZE_BYTES
    }
    LL = {
        "cache_name": "ll",
        "cache_size_bytes": DEFAULT_LL_SIZE_BYTES,
        "cache_default_assoc": DEFAULT_LL_ASSOC,
        "cache_line_size_bytes": DEFAULT_LL_LINE_SIZE_BYTES
    }


class Parameter(Enum):
    TOTAL_SIZE = {"desc": "Total Sizes", "id": "total_size"}
    ASSOC = {"desc": "Associativity", "id": "assoc"}
    LINE_SIZE = {"desc": "Line Sizes", "id": "line_size"}
    RESOLUTION = {"desc": "Resolutions", "id": "res"}


image_resolutions = {
    "144p": {
        "width": 256,
        "height": 144
    },
    "240p": {
        "width": 426,
        "height": 240
    },
    "360p": {
        "width": 480,
        "height": 360
    },
    "480p": {
        "width": 640,
        "height": 480
    },
    "720p": {
        "width": 1280,
        "height": 720
    },
    "1080p": {
        "width": 1920,
        "height": 1080
    },
    "4K": {
        "width": 3840,
        "height": 2160
    },
    "8K": {
        "width": 7680,
        "height": 4320
    },
}
########################################################################################################################
# CONSTANTS AND GLOBAL DEFINITIONS/OBJECTS  ############################################################################
########################################################################################################################


def get_all_hit_rates(valgrind_output_folder: str = "./valgrind_output_seq"):
    all_hit_rates = {}
    for root, dirs, files in os.walk(valgrind_output_folder):
        if os.path.basename(root) == os.path.basename(valgrind_output_folder):
            continue

        # Else fetch cache type and parameter, or resolution(have to check here)
        root_parts = os.path.basename(root).split('_')

        cache_type: CacheType = None
        param: Parameter = None
        if root_parts[0] == "resolution":
            param = Parameter.RESOLUTION
        else:
            # Set CacheType first
            to_check_cache_str = root_parts[0]
            if to_check_cache_str == "d1":
                cache_type = CacheType.D1
            elif to_check_cache_str == "i1":
                cache_type = CacheType.I1
            elif to_check_cache_str == "ll":
                cache_type = CacheType.LL
            else:
                return None

            # Now set Parameter that is being tested
            to_check_par_str = root_parts[1]
            if to_check_par_str == "assoc":
                param = Parameter.ASSOC
            elif to_check_par_str == "line":
                param = Parameter.LINE_SIZE
            elif to_check_par_str == "total":
                param = Parameter.TOTAL_SIZE
            else:
                return None

        # Okay, let's iterate over files now
        for file in files:
            extension_parts = os.path.splitext(file)
            if extension_parts[1] == ".out":
                continue

            hit_rates_file = get_hit_rates_for_file(os.path.join(root, file))

            graph_id = None
            if param is Parameter.RESOLUTION:
                res_id = os.path.basename(file).split('_')[0]
                # graph_id = f"{param}_{res_id}"
                graph_id = (param, res_id)
            else:
                # graph_id = f"{param}_{cache_type}"
                m = re.search(r".+=(\d+)_", os.path.basename(file))
                param_val = int(m.group(1))
                graph_id = (param, param_val, cache_type)

            all_hit_rates[graph_id] = hit_rates_file

    draw_graphs(all_hit_rates)


def draw_graphs(all_hit_rates: dict):
    os.makedirs(GRAPHS_SAVE_FOLDER, exist_ok=True)
    print("[GRAPH] Plotting resolutions...")

    # Time to move on to other graphs -> pairs x: (param, cache_type), y:aggregate data of 5 curves
    #                                       or x: (Resolution), y: aggregate data of 5 curves
    aggregated_data = defaultdict(list)
    for key, val in all_hit_rates.items():
        curr_param = key[0]
        curr_param_val = key[1]

        if curr_param is Parameter.RESOLUTION:
            res_num = int(curr_param_val[:-1])
            if res_num == 4:
                res_num = 2160
            elif res_num == 8:
                res_num = 4320
            aggregated_data[curr_param].append(GraphPair(x=res_num, y=val))
        else:
            curr_cache_type = key[2]
            aggregated_data[(curr_param, curr_cache_type)].append(GraphPair(x=curr_param_val, y=val))

    for key, val in aggregated_data.items():
        # Plot them now
        fig, ax = plt.subplots()

        x = []
        y_i1 = []
        y_d1 = []
        y_ll = []
        y_l_li = []
        y_l_ld = []

        for graph_pair in val:
            x_val, y_vect = graph_pair

            if len(y_vect) == 0:
                continue        # Valgrind couldnt collect data for this x

            x.append(x_val)
            y_i1.append(y_vect['I1'])
            y_d1.append(y_vect['D1'])
            y_ll.append(y_vect['LL'])
            y_l_li.append(y_vect['LLi'])
            y_l_ld.append(y_vect['LLd'])

        ax.plot(x, y_i1, 'o', label='I1')
        ax.plot(x, y_d1, 'o', label='D1')
        ax.plot(x, y_ll, 'o', label='LL')
        ax.plot(x, y_l_li, 'o', label='LLi')
        ax.plot(x, y_l_ld, 'o', label='LLd')

        x_label = ""
        if key is Parameter.RESOLUTION:
            # Time to graph the resolution
            x_label = key.value["desc"]
        else:
            x_label = f'{key[0].value["desc"]}_{key[1].value["cache_name"]}'

        ax.set(xlabel=x_label, ylabel='Hit rate(%)', title='Hit rate for various parameter and cache configurations')

        ax.legend()
        ax.grid()

        output_dir = f"{GRAPHS_SAVE_FOLDER}/{x_label}_graph.png"
        plt.savefig(output_dir)


def get_hit_rates_for_file(filename):
    print(f"[INFO] Getting hit rates for file: {filename}")
    # Need to open the file, and find the hit rate for the 3 cache_types
    hit_rates = {}
    with open(filename, 'r') as file:
        for line in file:
            if "miss rate" in line:
                # print(line, end='')
                cache_type = line[11:14]
                cache_type = cache_type.rstrip()

                # Now need to cut the hit percentage from the line
                res_m = re.search(HIT_PERCENTAGE_RE_PATTERN, line)
                res = res_m.group(0)[:-1]       # Omit last character(%)
                hit_rate = 100 - float(res)
                # print(f"Hit rate: {hit_rate}\n")
                hit_rates[cache_type] = hit_rate

    return hit_rates


def main():
    get_all_hit_rates("./valgrind_output_rand")


if __name__ == "__main__":
    main()
