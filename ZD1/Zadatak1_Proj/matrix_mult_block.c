#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char* argv[])
{
    double **mat1, **mat2, **result;
    int row1, col1, row2, col2;
    int i, j, k, ib, jb, kb;

    row1 = strtol(argv[1], NULL, 10);
    col1 = strtol(argv[2], NULL, 10);
    row2 = strtol(argv[3], NULL, 10);
    col2 = strtol(argv[4], NULL, 10);

//    printf("\nEnter number of rows for first matrix : ");
//    scanf("%d", &row1);
//    printf("\nEnter number of columns for first matrix : ");
//    scanf("%d", &col1);
//    printf("\nEnter number of rows for second matrix : ");
//    scanf("%d", &row2);
//    printf("\nEnter number of columns for second matrix : ");
//    scanf("%d", &col2);

    if(col1 != row2)
    {
        printf("\nCannot multiply two matrices.");
        return(0);
    }

    srand(time(NULL));

    /* Allocating memory for three matrix rows. */
    mat1 = (double **) malloc(sizeof(double *) * row1);
    mat2 = (double **) malloc(sizeof(double *) * row2);
    result = (double **) malloc(sizeof(double *) * col1);
    /* Allocating memory for the column of three matrices. */
    for( i=0; i < row1; i++ )
    {
        mat1[i] = (double *)malloc(sizeof(double) * col1);
    }
    for( i=0; i < row2; i++ )
    {
        mat2[i] = (double *)malloc(sizeof(double) * col2);
    }
    for( i=0; i < col1; i++ )
    {
        result[i] = (double *)malloc(sizeof(double) * row2);
    }

    /* members of first matrix. */
    printf("\nFirst matrix:\n");
    for( i=0; i < row1; i++ )
    {
        for( j=0; j < col1; j++ )
        {
            mat1[i][j] = (double)rand()/RAND_MAX;
            printf("%f\t", mat1[i][j]);
        }
        printf("\n");
    }
    /* members of second matrix. */
    printf("\nSecond matrix:\n");
    for( i=0; i < row2; i++ )
    {
        for( j=0; j < col2; j++ )
        {
            mat2[i][j] = (double)rand()/RAND_MAX;
            printf("%f\t", mat2[i][j]);
        }
        printf("\n");
    }

    /* Calculation begins for the resultant matrix. */
//    int blockSize = round(sqrt((64*1024)/3));
//    if(blockSize > sqrt((64*1024)/3))
//    {
//        blockSize -= 1;
//    }

    int blocksize = 0.5 *
    printf("%d\n", blockSize);
    /*
    for( ib=0; ib < row1; ib += blockSize )
    {
        for( jb=0; jb < col1; jb += blockSize )
        {
            for( kb=0; kb < col2; kb += blockSize )
            {
                for( i=0; i < blockSize; i++ )
                {
                    for( j=0; j < blockSize; j++ )
                    {
                        result[i][j] = 0;
                        for( k=0; k < blockSize; k++ )
                        {
                            result[ib+i][jb+j] += mat1[ib+i][kb+k] * mat2[kb+k][jb+j];
                        }
                    }
                }
            }
        }
    }
    */
    for( ib=1; ib <= row1; ib++ )
    {
        for( jb=1; jb <= col1; jb++ )
        {
            for( kb=1; kb <= col2; kb++ )
            {
                for( i=(ib-1)*blockSize; i <= ib*blockSize; i++)
                {
                    for( j=(jb-1)*blockSize; j <= jb*blockSize; j++)
                    {
                        result[i][j] = 0;
                        for( k=(kb-1)*blockSize; k <= kb*blockSize; k++)
                        {
                            result[i][j] += mat1[i][k] * mat2[k][j];
                        }
                    }
                }
            }
        }
    }

    /* Printing the contents of third matrix. */
    printf("\nResultant matrix :\n");
    for( i=0; i < col1; i++ )
    {
        for( j=0; j < row2; j++ )
        {
            printf("%f\t", result[i][j]);
        }
        printf("\n");
    }
    return 0;
}